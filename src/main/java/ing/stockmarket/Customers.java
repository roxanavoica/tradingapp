package ing.stockmarket;

public class Customers {

	private int ID;
	private String name;
	private int age;
	private String address;
	private char sex;
	private int phone;

	public int getID() {

		return ID;

	}

	public void setID(int iD) {

		ID = iD;

	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public int getAge() {

		return age;
	}

	public void setAge(int age) {

		this.age = age;

	}

	public String getAddress() {

		return address;

	}

	public void setAddress(String address) {

		this.address = address;

	}

	public char getSex() {

		return sex;

	}

	public void setSex(char sex) {

		this.sex = sex;

	}

	public int getPhone() {

		return phone;

	}

	public void setPhone(int phone) {

		this.phone = phone;

	}

}
